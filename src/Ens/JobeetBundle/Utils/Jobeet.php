<?php

namespace Ens\JobeetBundle\Utils;

class Jobeet
{
    static public function slugify($text)
    {
        $teste = 0;
        
        if($text == 'Développeur Web'){
               $teste = 1;
        }

      // replace non letter or digits by -
      $text = preg_replace('#[^\\pL\d]+#u', '-', $text);
     
      // trim
      $text = trim($text, '-');

      // transliterate
      if (function_exists('iconv'))
      {
        $text = @iconv('UTF-8', 'ASCII//TRANSLIT', $text);
        if($teste == 1){
          return $text;
       }
      }

      // lowercase
      $text = strtolower($text);

      // remove unwanted characters
      $text = preg_replace('#[^-\w]+#', '', $text);

      if (empty($text))
      {
        return 'n-a';
      }

      return $text;
    }
}